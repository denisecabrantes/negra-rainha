  <!--Main Layout-->
  <main>

    <div class="container">

      <!--Section: Team v.1-->
      <section class="text-center team-section">

        <!--Grid row-->
        <div class="row text-center">

          <!--Grid column-->
          <div class="col-md-12 mb-4" style="margin-top: 50px;">

            <div class="avatar mx-auto">
              <img src="<?= base_url('/assets/img/Denise_ft.jpg');?>" class="img-fluid rounded-circle z-depth-1" alt="First sample avatar image">
            </div>
            <h3 class="my-3 font-weight-bold">
              <strong>Denise Cristina da Conceição Abrantes</strong>
            </h3>
            <h6 class="font-weight-bold teal-text mb-4 text-warning ">Empreendedora, Professora e Palestrante</h6>

            <!--Facebook-->
            <a href="https://www.facebook.com/denise.abrantes" class="p-2 m-2 fa-lg fb-ic">
              <i class="fab fa-facebook-f grey-text"> </i>
            </a>

            <!--Instagram-->
            <a href="https://www.instagram.com/denise.negrarainha/" class="p-2 m-2 fa-lg ins-ic">
              <i class="fab fa-instagram grey-text"> </i>
            </a>

            <p class="mt-5"></p>

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Team v.1-->

      <!--Section: Tabs-->
      <section>

        <ul class="nav md-pills pills-default d-flex justify-content-center">
          
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel11" role="tab">
              <strong>Trabalho</strong>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel12" role="tab">
              <strong>My team</strong>
            </a>
          </li>-->
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel13" role="tab">
              <strong>Portfolio</strong>
            </a>
          </li>
        </ul>

        
        <!-- Tab panels -->
        <!--<div class="tab-content">
          <div class="tab-pane fade  show active" id="panel11" role="tabpanel">
            <br>

            <div class="row">

              <div class="col-md-12">

                <section class="text-center mb-5">

                  <div class="row mb-4">

                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(41).jpg');">

                        <div class="text-white text-center d-flex align-items-center rgba-blue-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Project title</strong>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem,
                              optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos.
                              Odit sed qui, dolorum!.</p>
                            <a class="btn btn-outline-white btn-sm">
                              <i class="fas fa-clone left"></i> View project</a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(14).jpg');">


                        <div class="text-white text-center d-flex align-items-center rgba-teal-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Project title</strong>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem,
                              optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos.
                              Odit sed qui, dolorum!.</p>
                            <a class="btn btn-outline-white btn-sm">
                              <i class="fas fa-clone left"></i> View project</a>
                          </div>
                        </div>
                      </div>
                    </div>


                  </div>



                  <div class="row">


                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(11).jpg');">


                        <div class="text-white text-center d-flex align-items-center rgba-green-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Project title</strong>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem,
                              optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos.
                              Odit sed qui, dolorum!.</p>
                            <a class="btn btn-outline-white btn-sm">
                              <i class="fas fa-clone left"></i> View project</a>
                          </div>
                        </div>
                      </div>
                    </div>



                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(42).jpg');">


                        <div class="text-white text-center d-flex align-items-center rgba-stylish-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Project title</strong>
                            </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem,
                              optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos.
                              Odit sed qui, dolorum!.</p>
                            <a class="btn btn-outline-white btn-sm">
                              <i class="fas fa-clone left"></i> View project</a>
                          </div>
                        </div>
                      </div>
                    </div>


                  </div>


                </section>


              </div>


            </div>


          </div>



          <div class="tab-pane fade" id="panel12" role="tabpanel">
            <br>


            <section id="team" class="section team-section pb-4">


               <h2 class="font-weight-bold text-center h1 my-5">Our amazing team</h2>

               <p class="text-center grey-text mb-5 mx-auto w-responsive">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam eum porro a pariatur accusamus veniam.</p>


               <div class="row mb-lg-4 text-center text-md-left">


                   <div class="col-lg-6 col-md-12 mb-4">

                       <div class="col-md-6 float-left">
                           <div class="avatar mx-auto mb-md-0 mb-3">
                               <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(27).jpg" class="z-depth-1" alt="First sample avatar image">
                           </div>
                       </div>

                       <div class="col-md-6 float-right">
                           <h4><strong>John Doe</strong></h4>
                           <h6 class="font-weight-bold grey-text mb-4">Web Designer</h6>
                           <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.</p>


                           <a class="p-2 m-2 fa-lg fb-ic"><i class="fab fa-facebook-f"> </i></a>

                           <a class="p-2 m-2 fa-lg tw-ic"><i class="fab fa-twitter"> </i></a>
    
                           <a class="p-2 m-2 fa-lg dribbble-ic"><i class="fab fa-dribbble"> </i></a>
                       </div>

                   </div>



                   <div class="col-lg-6 col-md-12 mb-4">

                       <div class="col-md-6 float-left">
                           <div class="avatar mx-auto mb-md-0 mb-3">
                               <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="z-depth-1" alt="Second sample avatar image">
                           </div>
                       </div>

                       <div class="col-md-6 float-right">
                           <h4><strong>Maria Kate</strong></h4>
                          <h6 class="font-weight-bold grey-text mb-4">Photographer</h6>
                           <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.</p>


                           <a class="p-2 m-2 fa-lg fb-ic"><i class="fab fa-facebook-f"> </i></a>

                           <a class="p-2 m-2 fa-lg yt-ic"><i class="fab fa-youtube"> </i></a>

                           <a class="p-2 m-2 fa-lg ins-ic"><i class="fab fa-instagram"> </i></a>
                       </div>

                   </div>


               </div>

               <div class="row text-center text-md-left">


                   <div class="col-lg-6 col-md-12 mb-4">

                       <div class="col-md-6 float-left">
                           <div class="avatar mx-auto mb-md-0 mb-3">
                               <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(26).jpg" class="z-depth-1" alt="Fourth sample avatar image">
                           </div>
                       </div>

                       <div class="col-md-6 float-right">
                           <h4><strong>Anna Deynah</strong></h4>
                           <h6 class="font-weight-bold grey-text mb-4">Web Developer</h6>
                           <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.</p>


                           <a class="p-2 m-2 fa-lg fb-ic"><i class="fab fa-facebook-f"> </i></a>

                           <a class="p-2 m-2 fa-lg tw-ic"><i class="fab fa-twitter"> </i></a>

                           <a class="p-2 m-2 fa-lg git-ic"><i class="fab fa-github"> </i></a>
                       </div>

                   </div>

                   <div class="col-lg-6 col-md-12 mb-4">
                       <div class="col-md-6 float-left">
                           <div class="avatar mx-auto mb-md-0 mb-3">
                               <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(29).jpg" class="z-depth-1" alt="Fifth sample avatar image">
                           </div>
                       </div>

                       <div class="col-md-6 float-right">
                           <h4><strong>Sarah Melyah</strong></h4>
                          <h6 class="font-weight-bold grey-text mb-4">Front-end Developer</h6>
                           <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.</p>

   
                           <a class="p-2 m-2 fa-lg gplus-ic"><i class="fab fa-google-plus-g"> </i></a>

                           <a class="p-2 m-2 fa-lg li-ic"><i class="fab fa-linkedin-in"> </i></a>

                           <a class="p-2 m-2 fa-lg email-ic"><i class="fas fa-envelope"> </i></a>
                       </div>

                   </div>

               </div>


            </section>

          </div>

          <div class="tab-pane fade" id="panel13" role="tabpanel">
            <br>
          -->
            <!-- Section: Team v.3 -->
            <section id="team" class="section team-section pb-4">

            <div class="list-group-flush" >
                <div class="list-group-item">
                    <p class="mb-0"><i class="far fa-image fa-2x mr-4  p-3 text-warning" aria-hidden="true"></i>
                    <strong>Formação</strong><br> 
                    Licenciatura Plena em Educação Artística / Artes Visuais - UNIFIG, Centro Universitário Metropolitano de São Paulo (concluído em dezembro de 2008). <br>
                    Pedagogia - Faculdade Alvorada, Taboão da Serra (concluído em dezembro de 2015).
                    </p>
                </div>
                <div class="list-group-item">
                    <p class="mb-0"> <i class="fas fa-briefcase fa-2x mr-4 mr-4 p-3  text-warning" aria-hidden="true"></i>
                    <strong>Cursos e Oficinas:</strong><br>
                    Oficina Itinerante de Vídeo Tela Brasil Módulo I e II, Educação e Tecnologia em Internet/SENAC/SP. <br>
                    Fotografia/Cine clube Bandeirantes SP. <br>
                    Oficina de Gestão em Projetos e Capitação de recursos. <br>
                    </p>
                </div>
                <div class="list-group-item">
                    <p class="mb-0"><i class="fas fa-anchor fa-2x mr-4 mr-4  p-3  text-warning" aria-hidden="true"></i>
                    <strong>Descrição do Profissional</strong><br>
                    A Experiência com arte educação iniciou-se no Instituto Diet – 2008, ministrando oficina de Desenho, Pintura,
                    Artesanato, Contação de histórias, Oficinas de Criação relacionando a Arte e o Meio Ambiente, 
                    Roda de conversas temáticas com Mulheres e Adolescentes desenvolvendo processo de criação individual, 
                    coletivo e promovendo exposições. Dando continuidade a esse trabalho nas demais ONGs:<br>
                    <ul>
                    <li>Casa de Cultura e Instituto de Promoção Social Água e Vida (De 2009 a 2013)</li>
                    <li>Associação Santa Emília (De 2014 a 1015)</li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
        </div>

        </div>

      </section>
      <!--Section: Tabs-->

    </div>

  </main>
  <!--Main Layout-->
