  <!--Main layout-->
  <main>
    <div class="container">

      <!--Section: Main info-->
      <section class="mt-5 wow fadeIn">

        <!--Grid row-->
        <div class="row">

          <!--Grid column-->
          <div class="col-md-6 mb-4">

            <img src="<?= base_url('/assets/img/logo_bxq.jpg');?>" class="img-fluid z-depth-1-half"
              alt="Logo da Marca Negra-Rainha">

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-md-6 mb-4">

            <!-- Main heading -->
            <h3 class="h3 mb-3 text-warning">Negra Rainha</h3>
            <p> A importância das mulheres negras por trás de uma 
              <strong>Luta</strong> e cortes de cabelo, impondo cada vez mais nossa cultura.</p>
            <p>Saia da Chapinha, mostre como nosso cacheados são maravilhosos.</p>


            <!-- Main heading -->

            <hr>
            <p>Cabelo duro? Não.<br>
            Meu cabelo é cacheado, livre, solto, macio, afro, encaracolado.<br>
            Duro é ter que conviver, e ainda ter que ouvir pessoas de pensamentos e valores tão ridículos e ultrapassados.</p>
            <p class="grey-text"> Thayná Andrade Silva Barreto </p>
            <hr>
            <p>A pior prisão é a do preconceito, da inveja, da cobiça, da hipocrisia, do egoísmo, da mentira, enfim a pior prisão é a mente...</p>
            <p class="grey-text"> Afro-X </p>

            <hr>
            <p>Se a luz do sol não para de brilhar...<br>
Se ainda existe noite
e luar... <br>
O mal não pode superar!!!</p>
            <p class="grey-text"> Denise C. C. Abrantes </p>

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Main info-->

      <hr class="my-5">

      <!--Section: Main features & Quick Start-->
      <section>

        <h3 class="h3 text-center mb-5 text-warning">Sobre Negra Rainha</h3>

        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-6 col-md-12 px-4">

            <!--First row-->
            <div class="row">
              <div class="col-1 mr-3">
                <i class="fas fa-hand-holding-heart fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h5 class="feature-title">Fundação</h5>
                <p class="grey-text">Aos quatorze (14) anos começei a minha empreitada no ramo dos cortes de cabelo, logo percebi que era essa a minha vocação, trabalhei duro e abri o meu salão Negra Rainha, onde conheci amigas, mães, companheiras, parceiras que levo pra vida.<br></p>
              </div>
            </div>
            <!--/First row-->

            <div style="height:30px"></div>

            <!--Second row-->
            <div class="row">
              <div class="col-1 mr-3">
                <i class="fas fa-people-carry fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h5 class="feature-title">Equipe</h5>
                <p class="grey-text"><strong>Gerente: </strong>Denise C. C. Abrantes
                </p>
                <p class="grey-text"><strong>Funcionária: </strong>Denise C. C. Abrantes
                </p><br>
                <p>Sim, gosto de trabalhar sozinha e com isso podemos gerar maior intimidade com o cliente, gerando um espaço confortavel de saude e beleza.</p>
              </div>
            </div>
            <!--/Second row-->

            <div style="height:30px"></div>

            <!--Third row-->
            <div class="row">
              <div class="col-1 mr-3">
                <i class="fas fa-plus fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h5 class="feature-title">Linhas de Acessórios e Roupas</h5>
                <p class="grey-text"> Hoje a Negra Rainha possui sua propria linha onde encontra-se grande variedade de roupas e acessórios.</p>
              </div>
            </div>


          </div>
          <!--/Grid column-->

          <!--Grid column-->
          <div class="col-lg-6 col-md-12">

            <p class="h5 text-center mb-4 text-warning">Tire um tempinho para ver os cortes afros:</p>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=DM3jH3OZJRM" allowfullscreen></iframe>
            </div>
            <p class="grey-text"><strong>Autor(a):</strong> Janyelle Morais

                </p>
          </div>
          <!--/Grid column-->

        </div>
        <!--/Grid row-->

      </section>
      <!--Section: Main features & Quick Start-->

      <hr class="my-5">

      <!--Section: Not enough-->
      <section>

        <h2 class="my-5 h3 text-center text-warning">Por que escolher a Negra Rainha?</h2>

        <!--First row-->
        <div class="row features-small mb-5 mt-3 wow fadeIn">

          <!--First column-->
          <div class="col-md-4">
            <!--First row-->
            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">Tempo no Mercado</h6>
                <p class="grey-text"> A Negra Rainha está no mercado informal a mais de dez (10) anos em tratamento de cabelo afro e um (1) ano e meio no ramo da moda feminina.
                </p>
                <div style="height:15px"></div>
              </div>
            </div>
            <!--/First row-->

            <!--Second row-->
            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">Foco</h6>
                <p class="grey-text">Nosso foco em moda afro feminina garante cortes e acessórios especificamente feito para nossas lindas mulheres Negras.
                </p>
                <div style="height:15px"></div>
              </div>
            </div>
            <!--/Second row-->

            <!--Third row-->
            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">Preço</h6>
                <p class="grey-text">Nossos preços são os melhores da região em todos os ramos da marca.</p>
                <div style="height:15px"></div>
              </div>
            </div>
            <!--/Third row-->

          </div>
          <!--/First column-->

          <!--Second column-->
          <div class="col-md-4 flex-center">
            <img src="<?= base_url('/assets/img/cacheia_ft1.jpg');?>" alt="MDB Magazine Template displayed on iPhone"
              class="z-depth-0 img-fluid">
          </div>
          <!--/Second column-->

          <!--Third column-->
          <div class="col-md-4 mt-2">
            <!--First row-->
            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">Missão</h6>
                <p class="grey-text">Nossa missão é realizar um sonho (uma vocação de vida).
                </p>
                <div style="height:15px"></div>
              </div>
            </div>
            <!--/First row-->

            <!--Second row-->
            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">Visão</h6>
                <p class="grey-text">Daqui a alguns anos expandir a empresa abrindo uma filial</p>
                <div style="height:15px"></div>
              </div>
            </div>
            <!--/Second row-->

            <!--Third row-->
            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x text-warning"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">Valores</h6>
                <p class="grey-text">Bom atendimento ao cliente, higiene no ambiente de trabalho e qualidade nos produtos oferecidos.
                </p>
                <div style="height:15px"></div>
              </div>
            </div>
            <!--/Third row-->

          </div>
          <!--/Third column-->

        </div>
        <!--/First row-->

      </section>
    </div>
  </main>