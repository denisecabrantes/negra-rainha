<div class="list-group-flush" >
  <div class="list-group-item">
    <p class="mb-0"><i class="far fa-image fa-2x mr-4 grey p-3 white-text rounded-circle " aria-hidden="true"></i>
    <strong>Formação</strong><br> 
      Licenciatura Plena em Educação Artística / Artes Visuais - UNIFIG, Centro Universitário Metropolitano de São Paulo (concluído em dezembro de 2008). <br>
      Pedagogia - Faculdade Alvorada, Taboão da Serra (concluído em dezembro de 2015).
    </p>
  </div>
  <div class="list-group-item">
    <p class="mb-0"> <i class="fas fa-briefcase fa-2x mr-4 mr-4 grey p-3 white-text rounded-circle" aria-hidden="true"></i>
    <strong>Cursos e Oficinas:</strong><br>
      Oficina Itinerante de Vídeo Tela Brasil Módulo I e II, Educação e Tecnologia em Internet/SENAC/SP. <br>
      Fotografia/Cine clube Bandeirantes SP. <br>
      Oficina de Gestão em Projetos e Capitação de recursos. <br>
    </p>
  </div>
  <div class="list-group-item">
    <p class="mb-0"><i class="fas fa-anchor fa-2x mr-4 mr-4 grey p-3 white-text rounded-circle" aria-hidden="true"></i>
    <strong>Descrição do Profissional</strong><br>
     A Experiência com arte educação iniciou-se no Instituto Diet – 2008, ministrando oficina de Desenho, Pintura,
     Artesanato, Contação de histórias, Oficinas de Criação relacionando a Arte e o Meio Ambiente, 
     Roda de conversas temáticas com Mulheres e Adolescentes desenvolvendo processo de criação individual, 
     coletivo e promovendo exposições. Dando continuidade a esse trabalho nas demais ONGs:<br>
     <ul>
      <li>Casa de Cultura e Instituto de Promoção Social Água e Vida (De 2009 a 2013)</li>
      <li>Associação Santa Emília (De 2014 a 1015)</li>
     </ul>
    </p>
  </div>
</div>