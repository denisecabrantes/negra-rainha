<div class="row wow fadeIn">
                    <div class="col-lg-5 col-xl-4 mb-4">

                        <div class="view overlay rounded z-depth-1">
                            <img src="" class="img-fluid" alt="">

                        </div>
                    </div>

                    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>Cortes de Cabelo</strong>
                        </h3>
                        <p class="grey-text">Uma das primeira perguntas a se fazer quando estamos prestes a criar um site é, por que essa e não aquela.</p>
                        <p>
                            <strong>Selecione o Corte que você tem interesse</strong>
                        </p>
                            <select>
                                <option value="1">Afro-1</option>
                                <option value="2">Afro-2</option>
                                <option value="3">Afro-3</option>
                                <option value="4">Afro-4</option>
                            </select>
                    </div>


</div>

<div class="card card-cascade narrower">

  <!-- Card image -->
  <div class="view view-cascade overlay">
    <img  class="card-img-top" src="https://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(147).jpg" alt="Card image cap">
    <a>
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body card-body-cascade">

    <!-- Label -->
    <h5 class="pink-text pb-2 pt-1"><i class="fas fa-utensils"></i> Afro-Clássico</h5>
    <!-- Title -->
    <h4 class="font-weight-bold card-title">Penteado Top</h4>
    <!-- Text -->
    <p class="card-text">O melhor do penteado cacheado em suas mãos.</p>
    <!-- Button -->
    <a class="btn btn-unique">Demonstrar Interesse</a>

  </div>

</div>



<!-- <div class="row wow fadeIn">
                    <div class="col-lg-5 col-xl-4 mb-4">

                        <div class="view overlay rounded z-depth-1">
                            <img src="" class="img-fluid" alt="">

                        </div>
                    </div>

                    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>Palestras</strong>
                        </h3>
                        <p class="grey-text">Uma das primeira perguntas a se fazer quando estamos prestes a criar um site é, por que essa e não aquela.</p>
                        <p>
                            <strong>A RV, possui os funcionários mais dedicados, além do vasto empenho em desenvolvimento web de forma rápida, eficaz e do jeitinho que o cliente gosta.</strong>
                        </p>
                        <option>
                    </div>


</div>

<div class="row wow fadeIn">
                    <div class="col-lg-5 col-xl-4 mb-4">

                        <div class="view overlay rounded z-depth-1">
                            <img src="" class="img-fluid" alt="">

                        </div>
                    </div>

                    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>Aulas de Português</strong>
                        </h3>
                        <p class="grey-text">Uma das primeira perguntas a se fazer quando estamos prestes a criar um site é, por que essa e não aquela.</p>
                        <p>
                            <strong>A RV, possui os funcionários mais dedicados, além do vasto empenho em desenvolvimento web de forma rápida, eficaz e do jeitinho que o cliente gosta.</strong>
                        </p>
                        <option>
                    </div>


</div>