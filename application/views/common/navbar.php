<nav class="mb-1 navbar navbar-expand-lg navbar-dark amber lighten-3">
<a class="navbar-brand" href="<?= base_url('index.php/home/historia'); ?>">
    <img src="<?= base_url('/assets/img/logo-nr.jpg')?>" height="30" alt="logo negra rainha">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333" >
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" style="color: black"  href="<?= base_url('index.php/home/historia'); ?>">História
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" style="color: black"  href="<?= base_url('index.php/home/biografia'); ?>">Biografia  
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" style="color: black"  href="<?= base_url('index.php/home/contatos'); ?>">Contatos
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" style="color: black" dropdown-toggle id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Lojinha
        </a>
        <div class="dropdown-menu dropdown-default navbar-dark amber lighten-3" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="<?= base_url('index.php/Desenvolvimento/precos1'); ?>">Acessesórios</a>
          <a class="dropdown-item" href="<?= base_url('index.php/Desenvolvimento/servicos'); ?>">Serviços</a>
        </div>
      </li>
      <li class="nav-item ">
        <a class="nav-link" style="color: black" href="<?= base_url('index.php/Usuario/criar'); ?>">Cadastre-se
        </a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="https://www.facebook.com/denise.abrantes" target="_blank">
        <i class="fab fa-facebook"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="https://www.instagram.com/denise.negrarainha/" target="_blank">
          <i class="fab fa-instagram"></i>
        </a>
      </li>
    
    </ul>
  </div>
</nav>
