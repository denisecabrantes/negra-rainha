  <footer class="page-footer footer-dark amber lighten-3 text-center font-small mt-8 wow fadeIn ">
    <hr class="my-3">
    <div class="footer-copyright py-5 amber accent-1 ">
    <p class="text-body">
      © 2019 Desenvolvido por : 
      <a href="http://hospedagem.ifspguarulhos.edu.br/~gu1701118/rv" target="_blank" class="text-body"> RV Networking </a>
    </p>
    </div>
  </footer>

  <script type="text/javascript" src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/js/popper.min.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/js/mdb.js') ?>"></script>
</body>

</html>