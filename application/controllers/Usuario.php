<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario extends CI_Controller {

	public function criar(){
		$this->load->view('common/header');
		$this->load->view('common/navbar');

        $this->load->view('usuario/cadastro_usuario');

		$this->load->view('common/footer');
	}

	private function show($content){
        $html = $this->load->view('common/header',null, true);
        $html .= $this->load->view('common/navbar',null, true);
        $html .= $content;
        $html .= $this->load->view('common/footer',null, true);
        echo $html;
    }

}
