<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	public function index(){
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('textos/TextoHistoria');
		$this->load->view('common/footer');
	}

	public function historia(){
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('textos/TextoHistoria');
		$this->load->view('common/footer');
	}

	public function biografia(){
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('textos/TextoBiografia');
		$this->load->view('common/footer');
	}

	public function contatos(){
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('textos/TextoContato');
		$this->load->view('common/footer');
	}


}
